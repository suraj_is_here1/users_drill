try {
    function staysInGermanny(users) {
        if (typeof (users) == "object") {
            let userList = Object.entries(users).filter(([user, userData]) => userData.nationality == "Germany")
                .map(([user, userData]) => user);

            return userList;
        }
        else {
            console.log("Pass Valid Arguments as parameter");
            return null;
        }
    }

} catch (error) {
    console.log("Somethig Went wrong !");

}
module.exports = staysInGermanny;