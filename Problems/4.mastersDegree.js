try {
    function masters(users) {
        if (typeof (users) == "object") {


            return Object.entries(users)
                .filter(([user, userData]) =>
                    userData.qualification == 'Masters'
                )
                .map(([user, userData]) => user);
        }
        else {
            console.log("Pass Valid Arguments as parameter");
            return null;
        }
    }

} catch (error) {
    console.log("Somethig Went wrong !");

}
module.exports = masters;