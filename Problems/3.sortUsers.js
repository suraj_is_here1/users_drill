try {
    function sortUserByDesignation(users) {
        if (typeof (users) == "object") {

            let userList = Object.entries(users).filter(([user, userData]) => userData.desgination.includes("Senior"))
                .sort(([user1, user1Data], [user2, user2Data]) => user2Data.age - user1Data.age);

            let userList2 = Object.entries(users).filter(([user, userData]) => (!userData.desgination.includes("Senior")) && userData.desgination.includes("Developer"))
                .sort(([user1, user1Data], [user2, user2Data]) => user2Data.age - user1Data.age);

            let userList3 = Object.entries(users).filter(([user, userData]) => userData.desgination.includes("Intern"))
                .sort(([user1, user1Data], [user2, user2Data]) => user2Data.age - user1Data.age);

            let finalUserList = userList.concat(userList2, userList3);

            return Object.fromEntries(finalUserList);
        }
        else {
            console.log("Pass Valid Arguments as parameter");
            return null;
        }
    }

} catch (error) {
    console.log("Somethig Went wrong !");
}
module.exports = sortUserByDesignation;
