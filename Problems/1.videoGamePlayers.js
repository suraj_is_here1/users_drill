try {
    function playsVideoGame(users) {
        if (typeof (users) == "object") {


            let userList = Object.keys(users).reduce((acc, user) => {

                let a = Object.entries(users[user])

                Object.keys(users[user]).forEach(key => {

                    if (key == "interest" || key == "interests") {
                        if (users[user][key][0].includes("Video Game")) {
                            acc.push(user);
                        }
                    }
                });
                return acc;
            }, []);

            return userList;
        }
        else {
            console.log("Pass Valid Arguments as parameter");
            return null;
        }
    }

} catch (error) {
    console.log("Somethig Went wrong !");

}
module.exports = playsVideoGame;