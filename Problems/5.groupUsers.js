try {
    function groupingProgrammers(users) {
        if (typeof (users) == "object") {

            let groupingByProgrammingLanguage = {}


            groupingByProgrammingLanguage["Python"] = Object.entries(users)
                .filter(([user, userData]) => userData.desgination.includes("Python"))
                .map(([user, userData]) => user);

            groupingByProgrammingLanguage["Golang"] = Object.entries(users)
                .filter(([user, userData]) => userData.desgination.includes("Golang"))
                .map(([user, userData]) => user);

            groupingByProgrammingLanguage["Javascript"] = Object.entries(users)
                .filter(([user, userData]) => userData.desgination.includes("Javascript"))
                .map(([user, userData]) => user);

            return groupingByProgrammingLanguage;
        }
        else {
            console.log("Pass Valid Arguments as parameter");
            return null;
        }
    }

} catch (error) {
    console.log("Somethig Went wrong !");

}
module.exports = groupingProgrammers;